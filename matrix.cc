#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <random>
#include <string>
#include <cstring>

template<class T>
struct Graph {
  using Store = std::map<T, std::set<T>>;
  void addEdge(T s, T d) {
    if (not s or not d)
      return;
    this->s[s].emplace(d);
  }
  int pathsWithLen(T src, T dst, size_t len) {
    int n = 0;
    countPaths(src, dst, len, n);
    return n;
  }
  template<class Os>
  void show(Os& os) {
    for(auto& p: s) {
      os << std::to_string(p.first) << " : ";
      for(auto& n: p.second)
        os << std::to_string(n) << "->";
      os << std::endl;
    }
  }
private:
  using Path = std::vector<T>;
  template<class Os>
  void putStrLn(size_t id, Path const& p, Os& os) {
    os << "path ("<< id <<") : ";
    for (auto& e: p)
      os << e << "->";
    os << std::endl;
  }
  void countPaths(T src, T dst, size_t len, int& n, Path const& path = Path()) {
    auto pth = path;
    pth.emplace_back(src);

    if (not s[src].size())
      return;
    if (not len and src == dst) {
      putStrLn(n, pth, std::cout);
      n++;
      return;
    }
    if (len) {
      for (auto nghb : s[src])
        countPaths(nghb, dst, len-1, n, pth);
    }
  }
  Store s;
};

template<class T, size_t R, size_t C>
struct Matrix {
  using G = Graph<T>;
  using Store = T[R*C];
  Matrix() {
    std::memset(s, 0, sizeof(s));
  }
  void initR(T mn, T mx) {
    std::random_device rd;
    std::default_random_engine g(rd());
    std::uniform_int_distribution<size_t> rnd_i(0, R-1);
    std::uniform_int_distribution<size_t> rnd_j(0, C-1);
    auto fill = [&] (T v)->bool {
      auto i = rnd_i(g);
      auto j = rnd_j(g);
      if (get(i,j))
        return false;
      set(i,j,v);
      return true;
    };
    for (auto v = mn; v<mx; v++) {
      auto trials = 10;
      while (not fill(v) and --trials)
        ;
    }
  }
  enum Dir {
    H, // horizontal
    V, // vertical
  };
  enum Dir complement(enum Dir v) {
    switch (v) {
    case H : return V;
    case V : return H;
    }
  }
  // move 1 or 2 steps in one direction, then move 2 or 1 steps in the other
  void makeGraph(G& g) {
    for(int i=0; i<R; i++)
      for(int j=0; j<C; j++)
        each(i, j, g);
  }
  struct Table {
    using Col = std::vector<std::string>;
    using Vec = std::vector<Col>;
    using Width = std::vector<size_t>;
    Table() : cols(C, Col()), colsW(C, 0) {}
    void value(int i, int j, T const& v) {
      auto vstr = std::to_string(v);
      colsW[j] = std::max(colsW[j], vstr.size());
      cols[j].emplace_back(vstr);
    }
    template<class Os>
    void showLine(Os& os) {
      for (size_t c=0; c< cols.size(); c++) {
        os << "-" << std::string(colsW[c], '-') << "-";
      }
      os << std::endl;
    }
    template<class Os>
    void show(Os& os) {
      for (size_t l=0; l<cols[0].size(); l++) {
        showLine(os);
        for (size_t c=0; c< cols.size(); c++) {
          auto str = cols[c][l];
          os << "|" << str << std::string(colsW[c]-str.size(),' ') << "|";
        }
        os << std::endl;
      }
      showLine(os);
    }
  private:
    Vec cols;
    Width colsW;
  };
  template<class Os>
  void show(Os& os) {
    Table tbl;
    for(int i=0; i<R; i++)
      for(int j=0; j<C; j++)
        tbl.value(i,j, get(i,j));
    tbl.show(os);
    os << std::endl;
  }
private:
  T const& get(int i, int j) const {
    return s[i*C+j];
  }
  void set(int i, int j, T v) {
    s[i*C+j] = v;
  }
  void _walk(int i, int j, G& g, int hs, int vs, enum Dir start, enum Dir last) {
    // check for boundaries and 0 values
    if (i<0 or j<0 or i>=R or j>=C or not get(i,j))
      return;
    if (i+hs<0 or j+vs<0 or i+hs>=R or j+vs>=C or not get(i+hs,j+vs))
      return;

    if (abs(vs) + abs(hs) == 3) {
      // we reached a destination
      g.addEdge(get(i,j), get(i+hs, j+vs));
      return;
    }

    // 1 move left to go
    if (abs(vs)+abs(hs)==2) {
      if (start==last) { // 2 straight moves so far
        if (last==V) {
          _walk(i, j, g, hs+1, vs, start, H); // Right
          _walk(i, j, g, hs-1, vs, start, H); // Left
        } else { // last==H
          _walk(i, j, g, hs, vs+1, start, V); // Down
          _walk(i, j, g, hs, vs-1, start, V); // Up
        }
      } else { // last != start
        if (last==V) {
          if (vs>0) // Down
            _walk(i, j, g, hs, vs+1, start, last);
          else // Up
            _walk(i, j, g, hs, vs-1, start, last);
        } else { // last==H
          if (hs>0)
            _walk(i, j, g, hs+1, vs, start, last);
          else
            _walk(i, j, g, hs-1, vs, start, last);
        }
      }
      return;
    }

    if (abs(vs)+abs(hs)==1) {
      // 2 moves left to go, define next moves
      if (last==H) {
        if (hs>0)
          _walk(i, j, g, hs+1, vs, start, last);
        else
          _walk(i, j, g, hs-1, vs, start, last);

        _walk(i, j, g, hs, vs+1, start, V);
        _walk(i, j, g, hs, vs-1, start, V);
        return;
      }

      if (last==V) {
        if (vs>0)
          _walk(i, j, g, hs, vs+1, start, last);
        else
          _walk(i, j, g, hs, vs-1, start, last);

        _walk(i, j, g, hs+1, vs, start, H);
        _walk(i, j, g, hs-1, vs, start, H);
        return;  
      }
      return;
    }
  }
  void each(int i, int j, G& g) {
    if (0==get(i,j))
      return;

    _walk(i, j, g, 0, 1, V, V);
    _walk(i, j, g, 1, 0, H, H);
    _walk(i, j, g, 0, -1, V, V);
    _walk(i, j, g, -1, 0, H, H);
  }
  Store s;
};


int main() {
  Matrix<int, 9, 15> m;
  m.initR(1, 130); // generate a random pad with values from 1 to 40
  m.show(std::cout);
  Graph<int> g;
  m.makeGraph(g); // generate a graph of all accessible nodes that are 3 steps away, with only 2 consecutive possible moves either Horizontally or Vertically
  // g.show(std::cout);
  std::cout << "calculating : g.pathsWithLen(9, 33, 3)" << std::endl;
  auto n = g.pathsWithLen(9, 33, 3);
  std::cout << "Number of paths=" << std::to_string(n) << std::endl;
  return 0;
}
